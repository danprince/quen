export const Defaults = {
  pixelsPerUnit: 1,
  width: window.innerWidth,
  height: window.innerHeight
};

export default { ...Defaults };
