class Keyboard {
  constructor() {
    this.element = null;
    this.pressed = new Set();
    this.onKeyDown = this.onKeyDown.bind(this);
    this.onKeyUp = this.onKeyUp.bind(this);
  }

  attach(element) {
    this.element = element;
    this.start();
  }

  detach() {
    this.stop();
    this.element = null;
  }

  start() {
    this.element.addEventListener("keydown", this.onKeyDown);
    this.element.addEventListener("keyup", this.onKeyUp);
  }

  stop() {
    this.element.removeEventListener("keydown", this.onKeyDown);
    this.element.removeEventListener("keyup", this.onKeyUp);
  }

  onKeyDown(event) {
    this.pressed.add(event.which);
  }

  onKeyUp(event) {
    this.pressed.delete(event.which);
  }

  isDown(key) {
    return this.pressed.has(key);
  }

  clear() {
    this.pressed.clear();
  }
}

export default new Keyboard();
