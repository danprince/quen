export { default as Keyboard } from "./Keyboard";
export { default as Mouse } from "./Mouse";
export { default as Gamepad } from "./Gamepad";
export { default as InputSystem } from "./InputSystem";
export { default as InputComponent } from "./InputComponent";
export { default as Buttons } from "./Buttons.json";
export { default as Keys } from "./Keys.json";

// Shortcuts
export { default as focus } from "./InputComponent";
