class Gamepad {
  constructor() {
    this.gamepad = null;
    this.onConnect = this.onConnect.bind(this);
    this.onDisconnect = this.onDisconnect.bind(this);
    this.poll = this.poll.bind(this);
  }

  attach() {
    window.addEventListener("gamepadconnected", this.onConnect);
    window.addEventListener("gamepaddisconnected", this.onDisconnect);
  }

  detach() {
    window.removeEventListener("gamepadconnected", this.onConnect);
    window.removeEventListener("gamepaddisconnected", this.onDisconnect);
  }

  onConnect(event) {
    this.gamepad = event.gamepad;
    this.poll();
  }

  onDisconnect(event) {
    this.gamepad = null;
  }

  poll() {
    let pads = navigator.getGamepads();
    let pad = pads[this.gamepad.index];
    if (pad) this.gamepad = pad;
  }

  isActive() {
    return this.gamepad !== null;
  }

  getSticks() {
    return this.gamepad.axes;
  }

  getLeftStick() {
    return this.gamepad.axes[0];
  }

  getRightStick() {
    return this.gamepad.axes[0];
  }

  isDown(index) {
    if (this.gamepad == null) return false;
    let button = this.gamepad.buttons[index];
    return button.pressed;
  }
}

export default new Gamepad();
