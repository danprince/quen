# Quen Input
Handling input from mouse, keyboard and gamepads.

```js
import * as Input from "quen-input";

// Set up the keyboard and gamepad bindings
let input = new InputSystem([
  { on: "key", key: 13, send: "jump" },
  { on: "pad", button: "A", send: "jump" }
]);

// Add an input system to your world
world.addSystem(input);

// Add an input component to any entities
player.add(
  Input.InputComponent()
);

// Listen for key events on an element
Input.Keyboard.attach(window);

// Listen for mouse events on an element
Input.Mouse.attach(canvas);

// Listen for game pad events
Input.Gamepad.attach();

// Or just attach the devices and query them directly
console.log(
  Input.Mouse.x,
  Input.Mouse.y,
  Input.Mouse.isDown(),
  Input.Keyboard.isDown(13),
  Input.Gamepad.isActive(),
  Input.Gamepad.isDown("A")
);
```

# Mouse
## `Mouse.attach(element)`
Add mouse event listeners to a specific element.

## `Mouse.detach()`
Remove mouse event listeners from the current element.

## `Mouse.isDown()`
Returns a boolean to show whether the mouse is currently pressed or not.

## `Mouse.x`
The current x coordinate of the mouse, relative to the element it is attached to.

## `Mouse.y`
The current y coordinate of the mouse, relative to the element it is attached to.

# Keyboard
## `Keyboard.attach(element)`
Add key event listeners to a specific element (usually you want `window`).

## `Keyboard.detach()`
Remove key event listeners from the current element.

## `Keyboard.isDown(code)`
Returns a boolean to show whether the key with the given code is pressed or not.

# Gamepad
## `Gamepad.attach()`
Add gamepad event listeners.

## `Gamepad.isActive()`
Returns a boolean to show whether there is an active gamepad.

## `Gamepad.poll()`
Update the state of the gamepad. You don't need this if you are using `InputSystem`.

## `Gamepad.getLeftStick()`
Returns a number that represents the current position of the left analog stick.

## `Gamepad.getRightStick()`
Returns a number that represents the current position of the right analog stick.

## `Gamepad.isDown(name)`
Returns a boolean to show whether the button with the given name is pressed or not.

## `Gamepad.Buttons`
An object mapping of gamepad button names to their respective codes.

# InputSystem
`InputSystem` is responsible for configuring input bindings and turning them into entity messages.

## `new InputSystem(bindings)`
Creates a new input system with the given bindings. Bindings have two forms:

```js
// keyboard bindings
{ on: "key", key: number, send: string }

// gamepad bindings
{ on: "pad", button: string, send: string }
```

# InputComponent
`InputComponent` marks an entity to receive input messages from any InputSystems in the world.
