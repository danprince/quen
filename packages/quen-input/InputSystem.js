import { System, Filters } from "../quen-engine";
import Keyboard from "./Keyboard";
import Mouse from "./Mouse";
import Gamepad from "./Gamepad";

class InputSystem extends System {
  constructor(bindings) {
    super();
    this.bindings = bindings;
    this.filter = Filters.requireAll("input");
  }

  update() {
    let events = [];

    if (Gamepad.isActive()) {
      Gamepad.poll();
    }

    for (let binding of this.bindings) {
      if (binding.on === "key") {
        if (Keyboard.isDown(binding.key)) {
          events.push(binding.send);
        }
      }

      if (binding.on === "mouse") {
        if (Mouse.pressed) {
          events.push(binding.send);
        }
      }

      if (binding.on === "pad") {
        if (Gamepad.isDown(binding.button)) {
          events.push(binding.send);
        }
      }
    }

    for (let entity of this.entities) {
      for (let event of events) {
        this.post(entity.id, event);
      }
    }

    // Clear the key states so that we don't process any more events until
    // the browser sends them. Might need this behind a flag for games
    // that don't have smooth movement.
    Keyboard.clear();
  }
}

export default InputSystem
