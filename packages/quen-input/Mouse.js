class Mouse {
  constructor() {
    this.element = null;
    this.x = 0;
    this.y = 0;
    this.pressed = false;
    this.scroll = { x: 0, y: 0, z: 0 };

    this.onMouseDown = this.onMouseDown.bind(this);
    this.onMouseUp = this.onMouseUp.bind(this);
    this.onMouseMove = this.onMouseMove.bind(this);
    this.onMouseWheel = this.onMouseWheel.bind(this);
  }

  attach(element) {
    this.element = element;
    this.start();
  }

  detach() {
    this.stop();
    this.element = null;
  }

  start() {
    this.element.addEventListener("mousedown", this.onMouseDown);
    this.element.addEventListener("mouseup", this.onMouseUp);
    this.element.addEventListener("mousemove", this.onMouseMove);
    this.element.addEventListener("wheel", this.onMouseWheel);
  }

  stop() {
    this.element.removeEventListener("mousedown", this.onMouseDown);
    this.element.removeEventListener("mouseup", this.onMouseUp);
    this.element.removeEventListener("mousemove", this.onMouseMove);
    this.element.removeEventListener("wheel", this.onMouseWheel);
  }

  onMouseDown(event) {
    this.pressed = true;
  }

  onMouseUp(event) {
    this.pressed = false;
  }

  onMouseMove(event) {
    let rect = this.element.getBoundingClientRect();
    this.x = event.clientX - rect.left;
    this.y = event.clientY - rect.top;
  }

  onMouseWheel(event) {
    this.scroll.x = event.deltaX;
    this.scroll.y = event.deltaY;
    this.scroll.z = event.deltaZ;
  }

  isDown() {
    return this.pressed === true;
  }
}

export default new Mouse();
