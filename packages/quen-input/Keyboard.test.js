import Keyboard from "./Keyboard";

describe("Keyboard", () => {
  let element = document.body;

  test("should not respond to events before attaching", () => {
    element.dispatchEvent(new KeyboardEvent("keydown", { which: 13 }));
    expect(Keyboard.isDown(13)).toBe(false);
  });

  test("should respond to key events after attaching", () => {
    Keyboard.attach(element);
    element.dispatchEvent(new KeyboardEvent("keydown", { which: 13 }));
    expect(Keyboard.isDown(13)).toBe(true);
    element.dispatchEvent(new KeyboardEvent("keyup", { which: 13 }));
    expect(Keyboard.isDown(13)).toBe(false);
  });
});
