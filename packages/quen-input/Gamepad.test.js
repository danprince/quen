import Gamepad from "./Gamepad";

// jsdom does not have GamepadEvent constructor
let GamepadEvent = type => {
  let event = new Event(type);
  event.gamepad = { active: true };
  return event;
};

// jsdom does not have navigator.gamePads
navigator.getGamepads = () => ({});

describe("Gamepad", () => {
  test("should only be active when a gamepad is connected", () => {
    expect(Gamepad.isActive()).toBe(false);

    Gamepad.attach();
    window.dispatchEvent(new GamepadEvent("gamepadconnected"));
    expect(Gamepad.isActive()).toBe(true);
    window.dispatchEvent(new GamepadEvent("gamepaddisconnected"));
    Gamepad.detach();

    expect(Gamepad.isActive()).toBe(false);
  });
});
