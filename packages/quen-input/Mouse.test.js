import Mouse from "./Mouse";

describe("Mouse", () => {
  let element = document.createElement("div");

  element.getBoundingClientRect = () => ({
    left: 50,
    top: 100
  });

  test("should not respond to events before attaching", () => {
    element.dispatchEvent(new MouseEvent("mousedown"));
    expect(Mouse.isDown()).toBe(false);
  });

  test("should respond to mouseup and mousedown events", () => {
    Mouse.attach(element);

    element.dispatchEvent(new MouseEvent("mousedown"));
    expect(Mouse.isDown()).toBe(true);
    element.dispatchEvent(new MouseEvent("mouseup"));
    expect(Mouse.isDown()).toBe(false);
  });

  test("should respond to mousemove events", () => {
    element.dispatchEvent(new MouseEvent("mousemove"));
    expect(Mouse.x).toBe(-50);
    expect(Mouse.y).toBe(-100);
  });
});
