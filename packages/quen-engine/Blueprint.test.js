import Blueprint from "./Blueprint";

class Foo {}
Foo.key = "foo";
class Bar {};
Bar.key = "bar";

describe("Blueprint", () => {
  test("should create a blueprint that spawns entities", () => {
    let Actor = new Blueprint(new Foo(), new Bar());
    let player = Actor.spawn();
    expect(player.has("foo")).toBe(true);
    expect(player.has("bar")).toBe(true);
  });

  test("should be able to extend a blueprint", () => {
    let Actor = new Blueprint(new Foo());
    let Player = Actor.extend(new Bar());
    let actor = Actor.spawn();
    let player = Player.spawn();
    expect(actor.has("foo")).toBe(true);
    expect(actor.has("bar")).toBe(false);
    expect(player.has("foo")).toBe(true);
    expect(player.has("bar")).toBe(true);
  });

  test("should use last component when there are duplicates", () => {
    class Position {
      constructor(x, y) {
        this.x = x;
        this.y = y;
      }
    }

    Position.key = "position";

    let Actor = new Blueprint(new Position(0, 0));
    let Player = Actor.extend(new Position(1, 1));
    let player = Player.spawn();
    expect(player.position).toEqual({ x: 1, y: 1 });
  });
});
