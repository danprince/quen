import World from "./World";

describe("World", () => {
  let MockSystem = () => ({
    onEnter: jest.fn(),
    onExit: jest.fn(),
    update: jest.fn()
  });

  test("should enter systems after they are added", () => {
    let world = new World();
    let system = MockSystem();
    world.addSystem(system);
    expect(system.onEnter).toHaveBeenCalled();
  });

  test("should exit systems after they are removed", () => {
    let world = new World();
    let system = MockSystem();
    world.addSystem(system);
    world.removeSystem(system);
    expect(system.onExit).toHaveBeenCalled();
  });

  test("should get an entity by id", () => {
    let world = new World();
    let entity = { id: 0 };
    world.addEntity(entity);
    expect(world.getEntity(0)).toBe(entity);
    expect(world.getEntity(1)).toBe(undefined);
  });

  test("should get all entities", () => {
    let world = new World();
    let foo = { id: 0 };
    let bar = { id: 1 };
    world.addEntity(foo);
    world.addEntity(bar);
    expect(world.getEntities()).toEqual([foo, bar]);
  });

  test("should deactivate entity when it is removed", () => {
    let world = new World();
    let foo = { id: 0, active: true };
    world.addEntity(foo);
    world.removeEntity(foo);
    expect(world.getEntities()).toEqual([{ id: 0, active: false }]);
  });

  test("should remove inactive entities", () => {
    let world = new World();

    let entity = { id: 0, active: false };
    world.addEntity(entity);
    world.removeInactiveEntities();
    expect(world.getEntities()).toEqual([]);
  });

  test("should add a dispatcher", () => {
    let world = new World();
    let dispatcher = jest.fn();
    world.addDispatcher(dispatcher);
    world.postMessage(0, "Enter", "test");
    world.dispatchMessages();

    expect(dispatcher).toHaveBeenCalledWith({
      to: 0,
      type: "Enter",
      payload: "test"
    });
  });

  test("should remove dispatcher", () => {
    let world = new World();
    let dispatcher = jest.fn();
    world.addDispatcher(dispatcher);
    world.removeDispatcher(dispatcher);
    world.postMessage(0, "Enter", "test");
    world.dispatchMessages();
    expect(dispatcher).not.toHaveBeenCalled();
  });

  test("should block a message in dispatcher", () => {
    let world = new World();

    let a = jest.fn(message => message.to === 1);
    let b = jest.fn();

    world.addDispatcher(a);
    world.addDispatcher(b);

    world.postMessage(0, "Enter", "test");
    world.dispatchMessages();

    expect(a).toHaveBeenCalledWith({ to: 0, type: "Enter", payload: "test" });
    expect(b).toHaveBeenCalledWith({ to: 0, type: "Enter", payload: "test" });
    a.mockClear();
    b.mockClear();

    world.postMessage(1, "Update", "nope");
    world.dispatchMessages();

    expect(a).toHaveBeenCalledWith({ to: 1, type: "Update", payload: "nope" });
    expect(b).not.toHaveBeenCalled();
  });

  test("should mutate message in dispatcher", () => {
    let world = new World();

    let a = jest.fn(message => {
      message.payload = "wat";
      return false;
    });

    let b = jest.fn();

    world.addDispatcher(a);
    world.addDispatcher(b);

    world.postMessage(0, "Enter", "test");
    world.dispatchMessages();

    expect(a).toHaveBeenCalledWith({ to: 0, type: "Enter", payload: "wat" });
    expect(b).toHaveBeenCalledWith({ to: 0, type: "Enter", payload: "wat" });
  });

  test("should dispatch enter message after entity is added", () => {
    let world = new World();
    let dispatcher = jest.fn();

    world.addDispatcher(dispatcher);
    world.addEntity({ id: 0, active: true });
    world.dispatchMessages();

    expect(dispatcher).toHaveBeenCalledWith({
      to: 0,
      type: "Enter",
      payload: undefined
    });
  });

  test("should dispatch exit message after entity is removed", () => {
    let world = new World();
    let dispatcher = jest.fn();
    let foo = { id: 0, active: true };

    world.addDispatcher(dispatcher);
    world.addEntity(foo);
    world.removeEntity(foo);
    world.dispatchMessages();

    expect(dispatcher).toHaveBeenLastCalledWith({
      to: 0,
      type: "Exit",
      payload: undefined
    });
  });

  test("should update systems when updating the world", () => {
    let world = new World();
    let system = MockSystem();

    world.addSystem(system);

    world.update(10);
    expect(system.update).toHaveBeenCalledWith(10, 0);
  });

  test("should exit all systems when destroying the world", () => {
    let world = new World();
    let system = MockSystem();
    world.addSystem(system);
    world.destroy();
    expect(system.onExit).toHaveBeenCalled();
  });

  test("should remove all entities when destroying the world", () => {
    let world = new World();
    let foo = { id: 0, active: true };
    world.addEntity(foo);
    world.destroy();
    expect(world.getEntities()).toEqual([]);
  });

  test("should not dispatch after destroying the world", () => {
    let world = new World();
    let dispatcher = jest.fn();

    world.addDispatcher(dispatcher);
    world.destroy();

    world.postMessage(0, "Hello");
    world.dispatchMessages();

    expect(dispatcher).not.toHaveBeenCalled();
  });
});
