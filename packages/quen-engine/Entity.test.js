import Entity from "./Entity";

describe("Entity", () => {
  test("should assign a tag", () => {
    let entity = new Entity("camera");
    expect(entity.tag).toBe("camera");
  });

  test("should generate unique ids", () => {
    let a = new Entity();
    let b = new Entity();
    expect(a.id).not.toBe(b.id);
  });

  test("should add a component to entity", () => {
    let entity = new Entity();
    entity.add({ name: "position", x: 10, y: 0 });
    expect(entity.position).toEqual({ name: "position", x: 10, y: 0 });
  });

  test("should check for presence of component", () => {
    let entity = new Entity();
    entity.add({ name: "foo" });
    expect(entity.has("foo")).toBe(true);
    expect(entity.has("bar")).toBe(false);
  });

  test("should remove a component", () => {
    let entity = new Entity();
    let position = { name: "position", x: 0, y: 0 };
    entity.add(position);
    expect(entity.has("position")).toBe(true);
    entity.remove(position);
    expect(entity.has("position")).toBe(false);
  });
});
