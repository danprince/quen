import System from "./System";

describe("System", () => {
  class MockSystem extends System {
    constructor() {
      super();
      this.enter = jest.fn();
      this.exit = jest.fn();
    }
  }

  class TestSystem extends System {}

  test("should throw if system uses world before entering", () => {
    let system = new MockSystem();
    expect(() => system.world).toThrow();
  });

  test("should throw if system uses world after exiting", () => {
    let system = new MockSystem();
    let world = {};
    system.onEnter(world);
    system.onExit(world);
    expect(() => system.world).toThrow();
  });

  test("should call enter when a system enters a world", () => {
    let system = new MockSystem();
    let world = {};
    system.onEnter(world);
    expect(system.enter).toHaveBeenCalled();
  });

  test("should call exit when a system exits a world", () => {
    let system = new MockSystem();
    let world = {};
    system.onExit();
    expect(system.exit).toHaveBeenCalled();
  });

  test("should add entity to world", () => {
    let system = new TestSystem();
    let world = { addEntity: jest.fn() };
    system.onEnter(world);
    system.addEntity({ id: 0 });
    expect(world.addEntity).toHaveBeenCalledWith({ id: 0 });
  });

  test("should remove an entity from world", () => {
    let system = new TestSystem();
    let world = { removeEntity: jest.fn() };
    system.onEnter(world);
    system.removeEntity({ id: 0 });
    expect(world.removeEntity).toHaveBeenCalledWith({ id: 0 });
  });

  test("should get all entities from world", () => {
    let system = new TestSystem();
    let world = { getEntities: () => [{ id: 0 }] };
    system.onEnter(world);
    let entities = system.getAllEntities({ id: 0 });
    expect(entities).toEqual([{ id: 0 }]);
  });

  test("should get entity by id from world", () => {
    let system = new TestSystem();
    let world = { getEntity: jest.fn(() => ({ id: 0 })) };
    system.onEnter(world);
    let entity = system.getEntityById(0);
    expect(world.getEntity).toHaveBeenCalledWith(0);
    expect(entity).toEqual({ id: 0 });
  });
});
