import Entity from "./Entity";

class Blueprint {
  constructor(...components) {
    this.components = components;
  }

  extend(...components) {
    return new Blueprint(...this.components, ...components);
  }

  spawn(...args) {
    let entity = new Entity(...args);
    entity.add(...this.components);
    return entity;
  }
}

export default Blueprint
