class World {
  constructor() {
    this.systems = [];
    this.entities = new Map();
    this.messages = [];
    this.dispatchers = [];
    this.ticks = 0;
  }

  addSystem(system) {
    this.systems.push(system);
    system.onEnter(this);
  }

  removeSystem(system) {
    system.onExit(this);
    this.systems = this.systems.filter(other => other !== system);
  }

  addEntity(entity) {
    this.entities.set(entity.id, entity);
    this.postMessage(entity.id, "Enter");
  }

  addEntities(entities) {
    for (let entity of entities) {
      this.entities.set(entity.id, entity);
    }

    for (let entity of entities) {
      this.postMessage(entity.id, "Enter");
    }
  }

  removeEntity(entity) {
    this.postMessage(entity.id, "Exit");
    entity.active = false;
  }

  removeEntities(entities) {
    for (let entity of entities) {
      entity.active = false;
    }

    for (let entity of entities) {
      this.postMessage(entity.id, "Exit");
    }
  }

  getEntity(id) {
    return this.entities.get(id);
  }

  getEntities() {
    return [...this.entities.values()];
  }

  removeInactiveEntities() {
    for (let entity of this.getEntities()) {
      if (entity.active === false) {
        this.entities.delete(entity.id);
      }
    }
  }

  addDispatcher(dispatcher) {
    this.dispatchers.push(dispatcher);
  }

  removeDispatcher(dispatcher) {
    this.dispatchers = this.dispatchers.filter(other => other !== dispatcher);
  }

  postMessage(to, type, payload) {
    this.messages.push({ to, type, payload });
  }

  dispatchMessages() {
    let messages = this.messages;

    for (let message of messages) {
      for (let dispatcher of this.dispatchers) {
        let handled = dispatcher(message);
        if (handled) break;
      }
    }

    this.messages = [];
  }

  start() {
    this.dispatchMessages();
  }

  update(deltaTime) {
    for (let system of this.systems) {
      if (system.update) {
        system.update(deltaTime, this.ticks);
      }
    }

    this.dispatchMessages();
    this.removeInactiveEntities();

    this.ticks += 1;
  }

  destroy() {
    for (let system of this.systems) {
      this.removeSystem(system);
    }

    for (let [id, entity] of this.entities) {
      this.removeEntity(entity);
    }

    this.dispatchMessages();
    this.removeInactiveEntities();

    this.dispatchers = [];
  }
}

export default World
