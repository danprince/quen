class System {

  /**
   * Before the system has entered a world this system has no world instance
   * so we provide a getter on the prototype which throws an helpful error.
   *
   * Once the system enters a world this property is overridden by a world
   * property on the system instance.
   */
  get world() {
    let name = this.constructor.name;
    console.warn(`It looks like you might be trying to use ${name} before it has been added to a world! Or maybe it's already been removed?`);
    throw new Error("System has not in a world!");
  }

  // Internal API

  onEnter(world) {
    Object.defineProperty(this, "world", {
      value: world,
      configurable: true
    });

    if (this.enter) this.enter();
  }

  onExit() {
    delete this.world;

    if (this.exit) this.exit();
  }

  // Public API

  get entities() {
    if (this.filter) {
      return this.filterEntities(this.filter);
    } else {
      return this.getAllEntities();
    }
  }

  addEntity(entity) {
    this.world.addEntity(entity);
  }

  removeEntity(entity) {
    this.world.removeEntity(entity);
  }

  getAllEntities() {
    return this.world.getEntities();
  }

  getEntityById(id) {
    return this.world.getEntity(id);
  }

  filterEntities(...filters) {
    return this.getAllEntities().filter(entity => {
      return filters.every(filter => filter(entity));
    });
  }

  findEntity(...filters) {
    return this.getAllEntities().find(entity => {
      return filters.every(filter => filter(entity));
    });
  }

  getEntitiesByTag(tag) {
    return this.getAllEntities().filter(entity => entity.tag === tag);
  }

  getEntityByTag(tag) {
    return this.getAllEntities().find(entity => entity.tag === tag);
  }

  post(to, type, payload) {
    this.world.postMessage(to, type, payload);
  }
}

export default System
