export function requireAll(...components) {
  return entity => components.every(component => entity.has(component));
}

export function requireAny(...components) {
  return entity => components.some(component => entity.has(component));
}

export function rejectAll(...components) {
  return entity => !components.every(component => entity.has(component));
}

export function rejectAny(...components) {
  return entity => !components.some(component => entity.has(component));
}

