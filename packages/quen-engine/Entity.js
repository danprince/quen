class Entity {
  constructor(tag) {
    this.tag = tag;
    this.id = Entity.uid++;
    this.active = true;
  }

  add(...components) {
    for (let component of components) {
      if (component.constructor !== Object) {
        this[component.constructor.key] = component;
      } else {
        this[component.name] = component;
      }
    }
  }

  remove(...components) {
    for (let component of components) {
      if (component.constructor !== Object) {
        delete this[component.constructor.key];
      } else {
        delete this[component.name];
      }
    }
  }

  has(name) {
    return name in this;
  }
}

Entity.uid = 0;

export default Entity
