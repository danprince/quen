import * as Filters from "./Filters";
import Entity from "./Entity";

describe("Filters", () => {
  let entities = [];
  let player = new Entity();
  let camera = new Entity();

  player.add({ name: "position", x: 0, y: 0 });
  player.add({ name: "sprite", id: "foo" });
  camera.add({ name: "position", x: 0, y: 0 });

  entities.push(player);
  entities.push(camera);

  test("requireAll", () => {
    let filter = Filters.requireAll("position", "sprite");
    expect(entities.filter(filter)).toEqual([player]);
  });

  test("requireAny", () => {
    let filter = Filters.requireAny("position", "sprite");
    expect(entities.filter(filter)).toEqual([player, camera]);
  });

  test("rejectAll", () => {
    let filter = Filters.rejectAll("position", "sprite");
    expect(entities.filter(filter)).toEqual([camera]);
  });

  test("rejectAny", () => {
    let filter = Filters.rejectAny("position", "sprite");
    expect(entities.filter(filter)).toEqual([]);
  });
});
