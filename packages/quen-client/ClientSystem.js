import { System } from "../quen-engine";

const defaults = {
  url: "ws://localhost:4040",
};

class ClientSystem {
  constructor(config=defaults) {
    this.config = config;
    this.socket = new WebSocket(config.url);

    this.onSocketOpen = this.onSocketOpen.bind(this);
    this.onSocketClose = this.onSocketClose.bind(this);
    this.onSocketError = this.onSocketError.bind(this);
    this.onSocketMessage = this.onSocketMessage.bind(this);
    this.dispatcher = this.dispatcher.bind(this);

    this.listen();
  }

  listen() {
    this.socket.addEventListener("open", this.onSocketOpen);
    this.socket.addEventListener("close", this.onSocketClose);
    this.socket.addEventListener("error", this.onSocketError);
    this.socket.addEventListener("message", this.onSocketMessage);
  }

  enter() {
    this.addDispatcher(this.dispatcher);
  }

  exit() {
    this.removeDispatcher(this.dispatcher);
    this.socket.close();
    this.socket.removeEventListener("open", this.onSocketOpen);
    this.socket.removeEventListener("close", this.onSocketClose);
    this.socket.removeEventListener("error", this.onSocketError);
    this.socket.removeEventListener("message", this.onSocketMessage);
  }

  dispatcher(message) {
    this.sendToServer(message);
    return true;
  }

  onSocketOpen(message) {
    console.info("ClientSystem: Socket opened!");
  }

  onSocketMessage(message) {
    // TODO: Load these entities into the local world
  }

  onSocketError(error) {
    console.error("ClientSystem: Error!", error);
  }

  onSocketClose(message) {
    console.info("ClientSystem: Socket closed!");
  }

  sendToServer(message) {
    let json = JSON.stringify(message);
    this.socket.send(json);
  }
}
