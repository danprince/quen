## Quen Client

Modules for building the client side of a game that runs on a remote server. Designed to be paired with `quen-server`.

Uses a websocket to send locally dispatched messages for the server to handle (e.g. input).

The server will send the current entities via the same socket.

## Optimistic Updates
It's possible to load in some of the same systems you are using at the server to achieve optimistic updates at the client (e.g. keep entities moving even if the server misses a few ticks).
