import { System } from "../quen-engine";

class RecordingSystem extends System {
  constructor() {
    super();
    this.events = [];
    this.dispatcher = this.dispatcher.bind(this);
    this.recording = false;
    this.lastEventTime = 0;
  }

  enter() {
    this.world.addDispatcher(this.dispatcher);
  }

  dispatcher(message) {
    if (this.recording) {
      let now = Date.now();
      let delta = now - this.lastEventTime;
      this.lastEventTime = now;
      this.events.push({ delta, message });
    }
  }

  start() {
    this.lastEventTime = Date.now();
    this.recording = true;
  }

  pause() {
    this.recording = false;
  }

  resume() {
    this.lastEventTime = Date.now();
    this.recording = true;
  }

  stop() {
    let events = this.events;
    let duration = events.reduce((time, event) => time + event.delta, 0);
    this.recording = false;
    this.events = [];
    return { events, duration };
  }
}

export default RecordingSystem
