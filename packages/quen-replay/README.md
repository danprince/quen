# Quen Replay

Experimental systems for recording and replaying games.

## Recording

```js
import { RecordingSystem } from "quen-replay";

// Set up a recording system to record events
let recorder = new RecordingSystem();

// Add it to a world
world.addSystem(recorder);

// Start recording
recorder.start();
recorder.pause();
recorder.resume()

// Finish recording
let recording = recorder.stop();
```

## Replaying

```js
import { ReplayingSystem } from "quen-replay";

// Load a recording
let recording = ...;

// Set up a replaying system
let replayer = new ReplayingSystem(recording);

// Add it to a world and the replay starts immediately
world.addSystem(replayer);

// Control playback
replayer.play();
replayer.pause();
replayer.resume();
replayer.stop();
replayer.reset();
```

## Caveats

You'll probably want to remove or disable any input systems before replaying game events.

Recordings don't store any entity data, so you'll need to make sure you also reset the world back to it's initial state before playing a recording.

