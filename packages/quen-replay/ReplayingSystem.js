import { System } from "../quen-engine";

class ReplayingSystem extends System {
  constructor(recording) {
    super();
    this.events = recording.events;
    this.replaying = false;
    this.time = 0;
    this.cursor = 0;
  }

  update() {
    if (this.replaying === false) {
      return;
    }

    let now = Date.now();
    let delta = now - this.time;
    let messages = [];

    if (this.peek().delta <= delta) {
      let event = this.next();
      messages.push(event.message);
      this.time = now;
    }

    while (!this.isDone() && this.peek().delta === 0) {
      let event = this.next();
      messages.push(event.message);
    }

    if (this.isDone()) {
      this.stop();
    }

    for (let message of messages) {
      this.post(message.to, message.type, message.payload);
    }
  }

  next() {
    return this.events[this.cursor++];
  }

  peek() {
    return this.events[this.cursor];
  }

  isDone() {
    return this.cursor >= this.events.length;
  }

  // Public API

  play() {
    this.time = Date.now();
    this.replaying = true;
  }

  pause() {
    this.replaying = false;
  }

  resume() {
    this.time = Date.now();
    this.replaying = true;
  }

  stop() {
    this.replaying = false;
  }

  reset() {
    this.cursor = 0;
    this.time = Date.now();
  }
}

export default ReplayingSystem
