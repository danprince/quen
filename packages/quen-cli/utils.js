let fs = require("fs-extra");
let path = require("path");
let chalk = require("chalk");

const icon = "▽ ";

function clearConsole() {
  process.stdout.write('\x1B[2J\x1B[0f');
}

function formatTime(ms) {
  if (ms < 1000) {
    return `${ms}ms`;
  } else {
    return `${(ms / 1000).toFixed(2)}s`;
  }
}

function formatFrame(text) {
  return text
    .split("\n")
    .map(line => {
      let parts = line.split(":");

      if (parts.length === 1) {
        parts[0] = chalk.red(parts[0]);
      } else {
        parts[0] = chalk.blue(parts[0] + ":");
      }

      return ["    ", ...parts].join("");
    })
    .join("\n");
}

module.exports = {
  icon,
  clearConsole,
  formatTime,
  formatFrame,
};
