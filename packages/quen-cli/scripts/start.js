let fs = require("fs");
let path = require("path");
let rollup = require("rollup");
let chalk = require("chalk");
let LiveServer = require("live-server");
let Config = require("../Config");
let Utils = require("../Utils");
let Rollup = require("../Rollup");

function start() {
  process.env.NODE_ENV = "development";

  let config = Config.createBuildConfig();
  let rollupConfig = Rollup.createDevConfig(config);

  let server = LiveServer.start({
    port: config.project.port,
    root: config.paths.output,
    logLevel: 0,
    open: false
  });

  server.on("listening", startCompiler);

  function startCompiler() {
    let watcher = rollup.watch(rollupConfig);
    let address = server.address();
    let name = config.project.name;
    let url = `http://localhost:${address.port}`;
    let icon = Utils.icon;

    Utils.clearConsole();
    console.log(` ${chalk.yellow(icon)} Starting development mode for ${chalk.yellow(name)}...`);

    watcher.on("event", event => {
      switch (event.code) {
        case "BUNDLE_START": {
          console.log(` ${chalk.yellow(icon)} ${name}`);
          console.log(`Compiling...`);
          break;
        }

        case "BUNDLE_END": {
          Utils.clearConsole();
          let duration = Utils.formatTime(event.duration);
          console.log(` ${chalk.green(icon)} ${name}`);
          console.log(` ${chalk.green(icon)} Serving on ${chalk.green.underline(url)}`);
          console.log(` ${chalk.green(icon)} Compiled in ${chalk.blue(duration)}`);
          console.log(``);
          console.log(chalk.white(` ${icon} Watching for changes...`));
          break;
        }

        case "ERROR": {
          Utils.clearConsole();
          let error = event.error;
          console.log(` ${chalk.red(icon)} ${chalk.red("Error")} ${event.error.message}`);

          if (error.code === "PARSE_ERROR") {
            let code = Utils.formatFrame(error.frame);
            let file = `./${path.relative(process.cwd(), error.loc.file)}`;
            console.log(` ${chalk.red(icon)} In ${chalk.underline(file)}`);
            console.log(``);
            console.log(code);
            console.log(``);
          }

          fs.writeFileSync(config.files.bundle, `
            document.body.innerHTML = \`
              <style>
              body { background: #010101; }
              .overlay {
                position: fixed;
                top: 20px;
                left: 20px;
                right: 20px;
                bottom: 20px;
                background: #111;
                border-radius: 5px;
                font-family: mononoki, monospace;
                color: white;
                padding: 12px;
                font-size: 14pt;
              }
              pre {
                font: inherit;
              }

              i {
                color: #b71515;
                font-style: normal;
              }
              </style>

              <div class="overlay">
                <i>▽ Quen</i> <strong>Compile Error</strong>
                <p>${error.message}</p>
                <pre>${error.frame}</pre>
              </div>
            \`;
          `);

          console.log(chalk.white(` ${icon} Watching for changes...`));

          break;
        }

        case "FATAL": {
          Utils.clearConsole();
          let error = event.error;
          console.log(` ${chalk.red(icon)} ${chalk.red("Fatal Error")} ${event.error.message}`);

          if (error.code === "PARSE_ERROR") {
            let code = Utils.formatFrame(error.frame);
            let file = `./${path.relative(process.cwd(), error.loc.file)}`;
            console.log(` ${chalk.red(icon)} In ${chalk.underline(file)}`);
            console.log(``);
            console.log(code);
          }

          console.log(``);
          process.exit(1);
        }
      }
    });
  }
}

module.exports = start;
