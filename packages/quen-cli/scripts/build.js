let fs = require("fs-extra");
let path = require("path");
let rollup = require("rollup");
let chalk = require("chalk");
let Config = require("../Config");
let Utils = require("../Utils");
let Rollup = require("../Rollup");

function build() {
  process.env.NODE_ENV = "production";

  let config = Config.createBuildConfig();
  let rollupConfig = Rollup.createProdConfig(config);
  let icon = Utils.icon;

  let inputOptions = {
    input: rollupConfig.input,
    plugins: rollupConfig.plugins
  };

  let outputOptions = rollupConfig.output;

  let startTime = Date.now();

  fs.emptyDirSync(config.paths.output);

  console.log(` ${chalk.yellow(icon)} ${config.project.name}`);
  console.log(` ${chalk.yellow(icon)} Building...\n`);

  rollup.rollup(inputOptions)
    .then(bundle => bundle.write(outputOptions))
    .then(done => {
      let endTime = Date.now();
      let duration = endTime - startTime;
      let dir = `./${path.relative(process.cwd(), config.paths.output)}`;
      console.log(` ${chalk.green(icon)} Compiled to ${chalk.green(dir)} in ${chalk.green(Utils.formatTime(duration))}`);
    })
    .catch(err => {
      console.log(` ${chalk.red(icon)} Error`);
      console.log(` ${chalk.red(icon)} ${err.message}`);
    });
}

module.exports = build;
