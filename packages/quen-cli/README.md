# Quen CLI

Tools for developing and building Quen projects.

```bash
# start a development server
quen start # (defaults to index.js)

# build this game
quen build # (defaults to index.js)
```

## TODO
- [x] Support commonjs modules with [rollup-plugin-commonjs](https://github.com/rollup/rollup-plugin-commonjs)
- [x] Support project.json file with game metadata
- [ ] Support bundle hashing
- [x] Add outer script which takes start/build as an argument
