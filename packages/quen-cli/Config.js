let path = require("path");
let fs = require("fs-extra");
let defaultProject = require("./defaultProject");

function createBuildConfig() {
  let root = path.resolve(__dirname, process.cwd());
  let localProject = readProjectConfig(root);
  let project = Object.assign({}, defaultProject, localProject);

  let paths = {
    root: root,
    output: path.resolve(root, project.output)
  };

  let files = {
    entry: path.resolve(paths.root, project.entry),
    bundle: path.resolve(paths.output, project.bundle),
    template: path.resolve(paths.root, project.template),
    html: path.resolve(paths.output, project.html)
  };

  return { project, paths, files };
}

function readProjectConfig(dir) {
  let file = path.join(dir, "project.json");
  let config = fs.readJsonSync(file, { throws: false });
  return config || {};
}

module.exports = {
  createBuildConfig,
  readProjectConfig
};
