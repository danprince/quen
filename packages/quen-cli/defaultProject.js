let path = require("path");

module.exports = {
  name: "Quen",
  entry: "index.js",
  output: "build",
  template: path.join(__dirname, "template.html"),
  bundle: "bundle.js",
  html: "index.html",
  port: 6060
};
