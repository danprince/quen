let resolve = require("rollup-plugin-node-resolve");
let glob = require("rollup-plugin-glob-import");
let html = require("rollup-plugin-generate-html-template");
let uglify = require("rollup-plugin-uglify");
let babel = require("rollup-plugin-babel");
let commonjs = require("rollup-plugin-commonjs");
let json = require("rollup-plugin-json");
let url = require("rollup-plugin-url");
let glsl = require("rollup-plugin-glsl");

function createDevConfig(settings) {
  return {
    input: settings.files.entry,
    output: {
      file: settings.files.bundle,
      format: "iife",
      sourcemap: true,
    },
    plugins: [
      glsl({ include: /\.(glsl|frag|vert)$/ }),
      resolve(),
      glob(),
      commonjs(),
      json(),
      url(),
      html({
        template: settings.files.template,
        target: settings.files.html
      })
    ]
  };
}

function createProdConfig(settings) {
  return {
    input: settings.files.entry,
    output: {
      file: settings.files.bundle,
      format: "iife",
      sourcemap: false,
    },
    plugins: [
      glsl({ include: /\.(glsl|frag|vert)$/ }),
      resolve(),
      glob(),
      commonjs(),
      json(),
      url(),
      babel({
        babelrc: false,
        presets: [["env", { modules: false }]],
        plugins: ["external-helpers"]
      }),
      html({
        template: settings.files.template,
        target: settings.files.html
      }),
      uglify.uglify(),
    ]
  };
}

module.exports = {
  createDevConfig,
  createProdConfig
};
