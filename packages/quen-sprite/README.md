# Quen Sprite

Load and manipulate sprites and atlases.

- [ ] Load images from atlases / sprites
- [ ] Extract specific sprites from atlases
- [ ] Scale / crop / move / rotate sprites
- [ ] Recolor sprites
- [ ] Add sprites to atlases
- [ ] Combine atlases
