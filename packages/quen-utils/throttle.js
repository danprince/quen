function throttle(fn, ms) {
  let lastCall = 0;

  return function throttled(...args) {
    let currentCall = Date.now();
    let elapsed = currentCall - lastCall;

    if (elapsed > ms) {
      fn(...args);
      lastCall = currentCall;
    }
  }
}

export default throttle
