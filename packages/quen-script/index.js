export { default as ScriptSystem } from "./ScriptSystem";
export { default as ScriptComponent } from "./ScriptComponent";

// Shortcuts
export { default as add } from "./ScriptComponent";
