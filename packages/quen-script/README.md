# Quen Script

```js
import * as Script from "quen-script";
import * as scripts from "./scripts/*";

world.addSystem(
  new Script.ScriptSystem(scripts)
);

player.add(
  Script.ScriptComponent(["Player"])
);

// Will call scripts.Player.Hello(player)
world.post(player.id, "Hello");
```
