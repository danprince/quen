import { System, Filters } from "../quen-engine";

class ScriptSystem extends System {
  constructor(scripts={}) {
    super();
    this.scripts = scripts;
    this.filter = Filters.requireAll("scripts");
    this.dispatcher = this.dispatcher.bind(this);
  }

  call(scriptId, methodName, args) {
    let script = this.scripts[scriptId];

    if (script) {
      let method = script[methodName];
      if (method) method(...args);
    } else {
      throw new Error(`Missing script '${scriptId}'`);
    }
  }

  enter() {
    this.world.addDispatcher(this.dispatcher);
    Object.assign(window, this.createPublicAPI());
  }

  exit() {
    this.world.removeDispatcher(this.dispatcher);
  }

  update() {
    for (let entity of this.entities) {
      if (!entity.has("scripts")) return;

      for (let id of entity.scripts.ids) {
        this.call(id, "Update", [entity]);
      }
    }
  }

  dispatcher(message) {
    let entity = this.getEntityById(message.to);

    if (entity && entity.has("scripts")) {
      for (let scriptId of entity.scripts.ids) {
        this.call(scriptId, message.type, [entity, message.payload]);
      }

      return true;
    }
  }

  createPublicAPI() {
    return {
      Post: this.post.bind(this),
      GetByTag: this.getEntityByTag.bind(this),
      GetById: this.getEntityById.bind(this),
      GetEntities: this.getAllEntities(this),
      FindEntity: this.findEntity.bind(this),
      FilterEntities: this.filterEntities.bind(this),
      Spawn: this.addEntity.bind(this),
      Destroy: this.removeEntity.bind(this)
    }
  }
}

export default ScriptSystem
