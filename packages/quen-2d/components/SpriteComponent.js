class SpriteComponent {
  constructor(id) {
    this.id = id;
  }
}

SpriteComponent.key = "sprite";

export default SpriteComponent
