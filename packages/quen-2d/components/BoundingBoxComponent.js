function BoundingBoxComponent(width=0, height=0) {
  return {
    name: "boundingBox",
    width,
    height
  }
}

export default BoundingBoxComponent
