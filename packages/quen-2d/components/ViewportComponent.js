class ViewportComponent {
  constructor(config) {
    this.x = config.x,
    this.y = config.y;
    this.w = config.w;
    this.h = config.h;
  }
}

ViewportComponent.key = "viewport";

export default ViewportComponent
