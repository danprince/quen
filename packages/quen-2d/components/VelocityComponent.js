class VelocityComponent {
  constructor({ x=0, y=0, rotation=0 }) {
    this.x = x;
    this.y = y;
    this.rotation = rotation;
  }
}

VelocityComponent.key = "velocity";

export default VelocityComponent
