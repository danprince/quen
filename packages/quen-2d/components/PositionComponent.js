class PositionComponent {
  constructor({ x=0, y=0 }) {
    this.x = x;
    this.y = y;
  }
}

PositionComponent.key = "position";

PositionComponent.attributes = {
  x: {
    type: "number",
    label: "X",
    default: 0,
  },
  y: {
    type: "number",
    label: "Y",
    default: 0,
  }
};

export default PositionComponent
