class TransformComponent {
  constructor({ x=0, y=0, rotation=0, scale=1 }) {
    this.x = x;
    this.y = y;
    this.rotation = rotation;
    this.scale = scale;
  }
}

TransformComponent.key = "transform";

export default TransformComponent
