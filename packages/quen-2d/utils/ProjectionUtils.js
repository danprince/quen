import Settings from "quen-settings";

export function screenToWorld(camera, x, y) {
  // Calculate the viewport offset
  let offsetX = camera.viewport.x * camera.viewport.w * Settings.width;
  let offsetY = camera.viewport.y * camera.viewport.h * Settings.height;

  // Convert these by the viewport's offset
  let screenX = x - offsetX;
  let screenY = y - offsetY;

  // Convert these screen coordinates into world coordinates
  let originWorldX = screenX / Settings.pixelsPerUnit;
  let originWorldY = screenY / Settings.pixelsPerUnit;

  // Translate these world coordinates by the camera's position
  let worldX = originWorldX + camera.position.x;
  let worldY = originWorldY + camera.position.y;

  return { x: worldX, y: worldY };
}

export function worldToScreen(camera, x, y) {
  let worldX = x;
  let worldY = y;

  // Translate these coordinates by the camera's position
  let originWorldX = worldX - camera.position.x;
  let originWorldY = worldY - camera.position.y;

  // Convert these world coordinates into screen coordinates
  let screenX = originWorldX * Settings.pixelsPerUnit;
  let screenY = originWorldY * Settings.pixelsPerUnit;

  // Calculate the viewport offset
  let offsetX = camera.viewport.x * camera.viewport.w * Settings.width;
  let offsetY = camera.viewport.y * camera.viewport.h * Settings.height;

  return { x: screenX - offsetX, y: screenY };
}

export function getWorldTranslation(camera) {
  let { position, viewport } = camera;

  return {
    x: (
      (viewport.x * viewport.w * Settings.width) -
      (position.x * Settings.pixelsPerUnit)
    ),
    y: (
      (viewport.y * viewport.h * Settings.height) -
      (position.y * Settings.pixelsPerUnit)
    )
  }
}
