import { System, Filters } from "quen-engine";

class PhysicsSystem extends System {
  constructor() {
    super();
    this.filter = Filters.requireAll("transform", "velocity");
  }

  update(entities) {
    this.transform.applyVelocity(velocity);
  }
}

export default PhysicsSystem
