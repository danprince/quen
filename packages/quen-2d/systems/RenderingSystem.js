import { System, Filters } from "quen-engine";
import { Mouse } from "quen-input";
import Settings from "quen-settings";
import * as Project from "../utils/ProjectionUtils";

class RenderingSystem extends System {
  constructor(config) {
    super();
    this.filter = Filters.requireAll("sprite", "position");
    this.ppu = Settings.pixelsPerUnit;
    this.canvas = document.createElement("canvas");
    this.ctx = this.canvas.getContext("2d");
    this.resize(Settings.width, Settings.height);
    this.spritesheet = config.spritesheet;
    this.image = new Image();
    this.image.src = this.spritesheet.url;
  }

  getDefaultCamera() {
    return {
      position: { x: 0, y: 0 },
      viewport: { x: 0.5, y: 0.5, w: this.width, h: this.height }
    }
  }

  getActiveCamera() {
    return this.getEntityByTag("camera") || this.getDefaultCamera();
  }

  update() {
    let camera = this.getActiveCamera();
    this.render(camera);
  }

  render(camera) {
    this.ctx.save();
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

    let origin = Project.getWorldTranslation(camera);

    this.ctx.translate(
      Math.floor(origin.x),
      Math.floor(origin.y)
    );

    for (let entity of this.entities) {
      let sprite = this.spritesheet.sprites[entity.sprite.id];

      this.ctx.drawImage(
        this.image,
        sprite.x * this.ppu,
        sprite.y * this.ppu,
        (sprite.w || 1) * this.ppu,
        (sprite.h || 1) * this.ppu,
        entity.position.x * this.ppu,
        entity.position.y * this.ppu,
        (sprite.w || 1) * this.ppu,
        (sprite.h || 1) * this.ppu
      );
    }

    this.ctx.restore();
  }
}

export default RenderingSystem
