class RequestAnimationFrameDriver {
  constructor(callback) {
    this.callback = callback;
    this.lastTick = Date.now();
    this.running = false;
    this.loop = this.loop.bind(this);
  }

  loop() {
    if (this.running) {
      requestAnimationFrame(this.loop);
    }

    let currentTick = Date.now();
    let elapsedTime = currentTick - this.lastTick;

    this.lastTick = currentTick;
    this.callback(elapsedTime);
  }

  stop() {
    this.running = false;
  }

  start() {
    this.running = true;
    this.loop();
  }
}

export default RequestAnimationFrameDriver
