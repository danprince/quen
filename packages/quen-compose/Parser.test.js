import * as Parser from "./Parser";

describe("Parser", () => {
  test("should parse a basic registry", () => {
    let registry = Parser.parse(`
      define man
        hp 10

      define magical
        effect "magic"

      define wizard extends man magical

      define wand extends magical
        holdable true
    `);

    expect(registry).toEqual({
      man: {
        id: "man",
        extends: [],
        components: {
          hp: [10]
        }
      },
      magical: {
        id: "magical",
        extends: [],
        components: {
          effect: ["magic"]
        }
      },
      wizard: {
        id: "wizard",
        extends: ["man", "magical"],
        components: {
          hp: [10],
          effect: ["magic"]
        }
      },
      wand: {
        id: "wand",
        extends: ["magical"],
        components: {
          holdable: [true],
          effect: ["magic"]
        }
      }
    });
  });

  test("should parse comments", () => {
    let registry = Parser.parse(`
      # this is a comment
      define foo
        tag 10
    `);

    expect(registry).toEqual({
      foo: {
        id: "foo",
        extends: [],
        components: {
          tag: [10]
        }
      }
    });
  });
});
