const TokenTypes = [
  { id: "define", pattern: /^define/ },
  { id: "extends", pattern: /^extends/ },
  { id: "string", pattern: /^".*?"/ },
  { id: "number", pattern: /^-?[0-9]+\.?[0-9]+/ },
  { id: "boolean", pattern: /^(true|false)/ },
  { id: "symbol", pattern: /^[a-zA-Z0-9\-]+/ },
  { id: "newline", pattern: /^\n+/ },
  { id: "comment", pattern: /^#.*/ },
  { id: "space", pattern: /^\s+/ },
];

function assertTokenType(token, type) {
  if (token.type !== type) {
    throw new Error(`Expected ${type} but got ${token.type}!`);
  }
}

export function tokenize(src) {
  let tokens = [];
  let cursor = 0;

  while (cursor < src.length) {
    let rest = src.slice(cursor);
    let matched = false;

    for (let type of TokenTypes) {
      let match = rest.match(type.pattern);

      if (match) {
        let result = match[0];
        cursor += result.length;
        tokens.push({ type: type.id, text: result });
        matched = true;
        break;
      }
    }

    if (!matched) {
      throw new SyntaxError(`No matching token @ ${cursor} "${rest[0]}"`);
    }
  }

  tokens = tokens
    .filter(token => token.type !== "space")
    .filter(token => token.type !== "comment")

  for (let token of tokens) {
    if (token.type === "string") {
      token.text = token.text.slice(1, -1);
    }

    if (token.type === "number") {
      token.text = Number(token.text);
    }

    if (token.type === "boolean") {
      token.text = (token.text === "true");
    }
  }

  return tokens;
}

export function tokensToSyntaxTree(tokens) {
  let ast = [];

  while (tokens.length) {
    let token = tokens.shift();

    if (token.type === "define") {
      let node = { type: "define", extends: [], properties: [] };

      let symbol = tokens.shift();
      assertTokenType(symbol, "symbol");
      node.name = symbol.text;

      if (tokens[0].type === "extends") {
        tokens.shift();

        while (tokens[0].type !== "newline") {
          let symbol = tokens.shift();
          assertTokenType(symbol, "symbol");
          node.extends.push(symbol.text);
        }
      }

      while (tokens[0] && tokens[0].type !== "define") {
        let symbol = tokens.shift();
        if (symbol.type === "newline") continue;
        assertTokenType(symbol, "symbol");
        let id = symbol.text;
        let args = [];

        while (tokens[0].type !== "newline") {
          let token = tokens.shift();
          args.push(token.text);
        }

        node.properties.push({ id, args });
      }

      ast.push(node);
    }
  }

  return ast;
}

export function syntaxTreeToRegistry(ast) {
  let registry = {};

  for (let node of ast) {
    if (node.type === "define") {
      let components = {};

      for (let property of node.properties) {
        components[property.id] = property.args;
      }

      for (let id of node.extends) {
        let parent = registry[id];
        Object.assign(components, parent.components);
      }

      registry[node.name] = {
        id: node.name,
        extends: node.extends,
        components
      };
    }
  }

  return registry;
}

export function parse(src) {
  let tokens = tokenize(src);
  let ast = tokensToSyntaxTree(tokens);
  let registry = syntaxTreeToRegistry(ast);
  return registry;
}
