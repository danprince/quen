# Quen Compose

Experimental parser for Quen's component definition DSL.

```
define weapon
  damage 10

define sword extends weapon
  damage

define fire-sword extends sword
  effect "fire"
  description "A firey sword"
```
