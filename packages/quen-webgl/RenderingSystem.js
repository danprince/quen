import { System, Filters } from "quen-engine";
import WebGLRenderer from "./WebGLRenderer";

class RenderingSystem extends System {
  constructor(config) {
    super();
    this.filter = Filters.requireAll("sprite", "position");
    this.canvas = document.createElement("canvas");
    this.context = this.canvas.getContext("webgl");
    this.renderer = new WebGLRenderer(this.context, config.atlas);
    this.width = 0;
    this.height = 0;

    this.resize(config.width, config.height);
  }

  resize(width, height) {
    this.canvas.width = this.width = width;
    this.canvas.height = this.height = height;
  }

  update() {
    let sprites = [];

    for (let entity of this.entities) {
      let sprite = {
        id: entity.sprite.id,
        x: entity.position.x,
        y: entity.position.y,
        scale: entity.has("scale") ? entity.scale.value : 1,
        rotation: entity.has("rotation") ? entity.rotation.value : 0,
      };

      sprites.push(sprite);
    }

    this.render(sprites);
  }

  render(sprites) {
    let camera = this.getEntityByTag("camera");
    this.renderer.draw(sprites, camera);
  }
}

export default RenderingSystem

