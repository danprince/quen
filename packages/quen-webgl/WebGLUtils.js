export function createShader(gl, type, src) {
  let shader = gl.createShader(type);
  gl.shaderSource(shader, src);
  gl.compileShader(shader);

  let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);

  if (success === false) {
    let info = gl.getShaderInfoLog(shader);
    let name = (type === gl.FRAGMENT_SHADER ? "Fragment" : "Vertex");
    gl.deleteShader(shader);
    console.warn(`${name} Shader`);
    throw new Error(info);
  }

  return shader;
}

export function createProgram(gl, vs, fs) {
  let program = gl.createProgram();
  gl.attachShader(program, vs);
  gl.attachShader(program, fs);
  gl.linkProgram(program);

  let success = gl.getProgramParameter(program, gl.LINK_STATUS);

  if (success === false) {
    let info = gl.getProgramInfoLog(program);
    gl.deleteProgram(program);
    throw new Error(info);
  }

  return program;
}

export function createProgramFromScripts(gl, scripts) {
  let vertex = createShader(gl, gl.VERTEX_SHADER, scripts.vertex);
  let fragment = createShader(gl, gl.FRAGMENT_SHADER, scripts.fragment);
  let program = createProgram(gl, vertex, fragment);
  return program;
}

