precision mediump float;

uniform sampler2D uImage;
uniform vec2 uTextureSize;

varying vec2 vTexCoord;

void main() {
  vec4 color = texture2D(uImage, vTexCoord);
  vec2 px = vec2(1.0, 1.0) / uTextureSize;

  if (color.a == 0.0) {
    discard;
  }

  gl_FragColor = color;
}
