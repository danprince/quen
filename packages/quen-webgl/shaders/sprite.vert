attribute vec2 aPosition;
attribute vec2 aTexCoord;

uniform mat3 uProjectMatrix;
uniform mat3 uViewMatrix;
uniform mat3 uModelMatrix;
uniform mat3 uTextureMatrix;

varying vec2 vTexCoord;

void main() {
  mat3 transform = uProjectMatrix * uViewMatrix * uModelMatrix;
  vec3 position = transform * vec3(aPosition, 1);
  gl_Position = vec4(position, 1);

  vec3 texCoord = uTextureMatrix * vec3(aTexCoord, 1);
  vTexCoord = texCoord.xy;
}
