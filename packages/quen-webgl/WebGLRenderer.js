import { mat3 } from "gl-matrix";
import * as WebGLUtils from "./WebGLUtils";
import * as shaders from "./shaders";

class WebGLRenderer {
  constructor(gl, atlas) {
    this.gl = gl;
    this.atlas = atlas;
    this.program = null;
    this.attributes = {};
    this.uniforms = {};
    this.textures = {};

    this.image = new Image();
    this.image.src = atlas.url;
    this.image.addEventListener("load", () => this.setup());
    this.ready = false;
  }

  setup() {
    let gl = this.gl;

    this.program = WebGLUtils.createProgramFromScripts(this.gl, {
      vertex: shaders.spriteVert,
      fragment: shaders.spriteFrag
    });

    gl.useProgram(this.program);

    this.buffers = {
      position: gl.createBuffer(),
      texture: gl.createBuffer(),
    };

    this.attributes = {
      position: gl.getAttribLocation(this.program, "aPosition"),
      texCoord: gl.getAttribLocation(this.program, "aTexCoord"),
    };

    this.uniforms = {
      modelMatrix: gl.getUniformLocation(this.program, "uModelMatrix"),
      textureMatrix: gl.getUniformLocation(this.program, "uTextureMatrix"),
      projectMatrix: gl.getUniformLocation(this.program, "uProjectMatrix"),
      viewMatrix: gl.getUniformLocation(this.program, "uViewMatrix"),
      image: gl.getUniformLocation(this.program, "uImage"),
      textureSize: gl.getUniformLocation(this.program, "uTextureSize"),
    };

    let rect = [
      0, 0, 0, 1, 1, 0,
      1, 1, 0, 1, 1, 0,
    ];

    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.position);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(rect), gl.STATIC_DRAW);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.texture);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(rect), gl.STATIC_DRAW);

    this.textures = {
      sprites: gl.createTexture()
    };

    gl.bindTexture(gl.TEXTURE_2D, this.textures.sprites);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.image);

    this.ready = true;
  }

  draw(sprites, camera) {
    if (this.ready === false) return;

    let gl = this.gl;
    let width = gl.drawingBufferWidth;
    let height = gl.drawingBufferHeight;

    gl.viewport(0, 0, width, height);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(this.gl.COLOR_BUFFER_BIT);
    gl.enable(gl.BLEND);
    gl.disable(gl.DEPTH_TEST);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

    for (let sprite of sprites) {
      let base = this.atlas.sprites[sprite.id];
      let rescale = [this.atlas.spriteWidth, this.atlas.spriteHeight];
      let normalize = [1 / this.atlas.sheetWidth, 1 / this.atlas.sheetHeight];

      let modelMatrix = mat3.create();
      mat3.scale(modelMatrix, modelMatrix, rescale);
      mat3.translate(modelMatrix, modelMatrix, [sprite.x, sprite.y]);
      mat3.translate(modelMatrix, modelMatrix, [0.5, 0.5]);
      mat3.rotate(modelMatrix, modelMatrix, sprite.rotation);
      mat3.translate(modelMatrix, modelMatrix, [-0.5, -0.5]);

      let textureMatrix = mat3.create();
      mat3.translate(textureMatrix, textureMatrix, [base.x / this.atlas.sheetWidth, base.y / this.atlas.sheetHeight]);
      mat3.scale(textureMatrix, textureMatrix, [base.w / this.atlas.sheetWidth, base.h / this.atlas.sheetHeight]);

      let projectMatrix = mat3.create();
      mat3.scale(projectMatrix, projectMatrix, rescale);

      let viewMatrix = mat3.create();
      mat3.translate(viewMatrix, viewMatrix, [-camera.position.x, -camera.position.y]);
      mat3.scale(viewMatrix, viewMatrix, rescale);
      //mat3.scale(viewMatrix, viewMatrix, [camera.scale.value, camera.scale.value]);
      //mat3.rotate(viewMatrix, viewMatrix, camera.rotation.value);

      gl.enableVertexAttribArray(this.attributes.position);
      gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.position);
      gl.vertexAttribPointer(this.attributes.position, 2, gl.FLOAT, false, 0, 0);

      gl.enableVertexAttribArray(this.attributes.texCoord);
      gl.bindBuffer(gl.ARRAY_BUFFER, this.buffers.texture);
      gl.vertexAttribPointer(this.attributes.texCoord, 2, gl.FLOAT, false, 0, 0);

      gl.uniformMatrix3fv(this.uniforms.modelMatrix, false, modelMatrix);
      gl.uniformMatrix3fv(this.uniforms.textureMatrix, false, textureMatrix);
      gl.uniformMatrix3fv(this.uniforms.projectMatrix, false, projectMatrix);
      gl.uniformMatrix3fv(this.uniforms.viewMatrix, false, viewMatrix);
      gl.uniform2fv(this.uniforms.textureSize, rescale);

      gl.drawArrays(gl.TRIANGLES, 0, 6);
    }
  }
}

export default WebGLRenderer
