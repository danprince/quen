# Quen Server

Modules for building the server side of a game. Designed to be paired with `quen-client`.

Uses a websocket server to keep clients in sync. Clients will send input messages over their sockets so that the logic can happen here on the server. The server will sync the appropriate entities with each client.

Should be able to configure the server a few ways:

## Tick Rate
By default the server can tick with the world, but that's likely to eat a lot of bandwidth.

It should also be possible to set the server to a lower tick rate, or have it tick on receiving a custom event.

## Component Whitelist
Clients might only need to know about the components they will need to render the entities, so you might want to omit some of the components to save on bandwidth.

## Entity Hashing
If we can figure out a fast way to hash entities, we could reduce the socket payloads by only sending the entities whose components have updated.

