import { System } from "../quen-engine";

class LoggingSystem extends System {
  constructor(config) {
    super();
    this.config = config;
    this.dispatcher = this.dispatcher.bind(this);
  }

  enter() {
    this.world.addDispatcher(this.dispatcher);
  }

  exit() {
    this.world.removeDispatcher(this.dispatcher);
  }

  dispatcher(message) {
    let tag = `[${message.type}:${message.to}]`;

    if (message.payload === undefined) {
      console.log(tag);
    } else {
      console.log(tag, message.payload);
    }
  }
}

export default LoggingSystem
