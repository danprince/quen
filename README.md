# Quen (ECS Engine)

## Package Ideas
quen-text for roguelikes and text based adventures

## Starter Package
quen-starter package with sane defaults
maybe quen-go or quen-play is less patronising?
Should create a world set up with scripting system and renderer.

## Scoped Dispatchers
Allow dispatchers to register for a specific message type (or wildcard)

## Source Transforms
babel-plugin-transform-class-properties
rollup-plugin-postcss
rollup-plugin-url
rollup-plugin-copy-glob (public dir)

## Spritesheet
Should the renderer know about the spritesheet? Or can 
Pixels per unit

## Component Registry
Expose a `register` function that adds a component to a registry that can be used to serialize and parse the world.

```js
import { register, registry } from "quen-save";

export default register(MyComponent);
// or
export default register(MyComponent, "someName");

// added to registry
registry.someName === MyComponent;
```

Think this is actually a better approach than making all components classes. Just using a simple wrapper (maybe not even called register)

```js
function speed() {

}

export default component("speed", speed);

function component(name, factory) {
  if (name in registry) {
    throw new Error();
  }

  let builder = (...args) => {
    let value = factory(...args);
    return { name, value };
  };

  registry[name] = builder;

  return builder;
}
```

Can warn if two components are registered with one name. This could potentially be done as a babel plugin? Not sure how it would target the correct classes.

## Component Editor
Create Aseprite style dialogues that can edit components with attributes

```js
TransformComponent.attributes = {
  x: {
    type: "number",
    label: "X",
    default: 0,
  },
  enabled: {
    type: "boolean",
    label: "Enabled",
    default: false
  }
};

<ComponentEditor
  attributes={SomeComponent.attributes}
  value={someComponentInstance}
  onChange={newValues => {
    Object.assign(someComponentInstance);
  }}
```

## Perspective Projection
Could try including multiple projections (isometric etc)

## Viewport Decoupling
Should try and make sure that the scene doesn't know anything about the size of the client/renderer.
Have all world coordinates translated into viewport coordinates.
Set camera's origin using viewport coordinates.

## Yield To Driver
World#loop could be a generator that the driver implementations can control. Loop goes as many times as it needs before it is ready to yield control back to the driver.

## Inspiration
Good architecture
https://github.com/munificent/hauberk

Good ideas for an ActorSystem that keeps track of a queue of actions
http://journal.stuffwithstuff.com/2014/07/15/a-turn-based-game-loop/

