class TransformComponent {
  constructor({ x=0, y=0, rotation=0, scaling=1 }) {
    this.x = x;
    this.y = y;
    this.rotation = rotation;
    this.scaling = scaling;
  }

  move(x, y) {
    this.x += x;
    this.y += y;
  }

  rotate(x) {
    this.rotation += x;
  }

  scale(x) {
    this.scaling *= x;
  }
}

TransformComponent.key = "transform";

export default TransformComponent
