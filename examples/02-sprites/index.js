import { World, Entity } from "quen-engine";
import * as Drivers from "quen-drivers";
import spriteAtlas from "./spriteAtlas";
import { mat3 } from "gl-matrix";

let canvas = document.createElement("canvas");
let ctx = canvas.getContext("2d");
let mouse = { x: 0, y: 0 };

canvas.width = 500;
canvas.height = 500;

let image = new Image();
image.src = spriteAtlas.url;

let world = new World();

let entities = {
  man: { x: 10, y: 10, id: "man" },
  sword: { x: 10, y: 10, rotation: 0, id: "sword" }
};

let driver = new Drivers.RequestAnimationFrameDriver(delta => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  drawSprite(entities.man);
  drawSprite(entities.sword);

  let p1 = entities.man;
  let p2 = mouse;
  let angle = Math.atan2(p2.y - p1.y, p2.x - p1.x) + Math.PI / 2;
  entities.sword.rotation = angle;
  entities.sword.x = entities.man.x + Math.sin(angle);
  entities.sword.y = entities.man.y - Math.cos(angle);
});

window.addEventListener("keydown", event => {
  switch (event.key) {
    case "w":
      entities.man.y -= 0.1;
      break;
    case "a":
      entities.man.x -= 0.1;
      break;
    case "s":
      entities.man.y += 0.1;
      break;
    case "d":
      entities.man.x += 0.1;
      break;
  }
});

canvas.addEventListener("mousemove", event => {
  let rect = canvas.getBoundingClientRect();
  let mouseX = event.clientX - rect.left;
  let mouseY = event.clientY - rect.top;
  let worldX = mouseX / spriteAtlas.spriteWidth;
  let worldY = mouseY / spriteAtlas.spriteHeight;

  mouse = { x: worldX, y: worldY };
});

function drawSprite(entity) {
  let sprite = spriteAtlas.sprites[entity.id];
  let { spriteWidth, spriteHeight } = spriteAtlas;
  ctx.save();

  ctx.scale(spriteWidth, spriteHeight);
  ctx.translate(entity.x, entity.y);
  ctx.translate(0.5, 0.5);
  ctx.rotate(entity.rotation);
  ctx.translate(-0.5, -0.5);

  ctx.drawImage(
    image,
    sprite.x * spriteWidth,
    sprite.y * spriteHeight,
    sprite.w * spriteWidth,
    sprite.h * spriteHeight,
    0,
    0,
    sprite.w,
    sprite.h
  );

  ctx.restore();
}


driver.start();
document.body.appendChild(canvas);

