import url from "./sprites.png";

export default {
  url,
  sheetWidth: 4,
  sheetHeight: 4,
  spriteWidth: 16,
  spriteHeight: 16,
  sprites: {
    man: { x: 0, y: 0, w: 1, h: 1 },
    sword: { x: 1, y: 0, w: 1, h: 1 },
  }
}
