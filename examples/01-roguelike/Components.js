import db from "./database";

export function Symbol(character) {
  return {
    name: "symbol",
    character
  }
}

export function Position(x=0, y=0) {
  return {
    name: "position",
    x,
    y
  }
}

export function Color(foreground="white", background="transparent") {
  return {
    name: "color",
    foreground,
    background
  }
}

export class Grid {
  constructor(config) {
    this.width = config.width;
    this.height = config.height;
    this.cells = config.cells;
  }

  at(x, y) {
    let cell = this.cells[y][x];
    let id = db.floor[cell];
    return db.tiles[id];
  }
}

Grid.key = "grid";
