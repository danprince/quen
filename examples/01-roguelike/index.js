import * as Engine from "quen-engine";
import * as Input from "quen-input";
import * as Script from "quen-script";
import * as Systems from "./Systems";
import * as Components from "./Components";
import * as Scripts from "./Scripts";

let world = new Engine.World();

let rendering = new Systems.RenderingSystem({
  width: 160,
  height: 160,
  scale: 16,
  background: "#1f1f1f"
});

let input = new Input.InputSystem([
  { on: "key", key: Input.Keys.H, send: "MoveLeft" },
  { on: "key", key: Input.Keys.J, send: "MoveDown" },
  { on: "key", key: Input.Keys.K, send: "MoveUp" },
  { on: "key", key: Input.Keys.L, send: "MoveRight" },
  { on: "pad", button: Input.Buttons.LEFT, send: "MoveLeft" },
  { on: "pad", button: Input.Buttons.DOWN, send: "MoveDown" },
  { on: "pad", button: Input.Buttons.UP, send: "MoveUp" },
  { on: "pad", button: Input.Buttons.RIGHT, send: "MoveRight" },
]);

let script = new Script.ScriptSystem(Scripts);

world.addSystem(input);
world.addSystem(script);
world.addSystem(rendering);

let player = new Engine.Entity("player");

player.add(
  Components.Position(1, 1),
  Components.Color("white"),
  Components.Symbol("@"),
  Input.focus(),
  Script.add(["Player"]),
);

world.addEntity(player);

let enemy = new Engine.Entity();

enemy.add(
  Components.Position(5, 5),
  Components.Color("red"),
  Components.Symbol("A"),
);

world.addEntity(enemy);

let map = new Engine.Entity("map");

map.add(new Components.Grid({
  width: 10,
  height: 10,
  cells: [
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
    [2, 1, 1, 1, 0, 0, 4, 4, 4, 2],
    [2, 1, 1, 1, 0, 0, 0, 4, 4, 2],
    [2, 1, 1, 1, 0, 0, 0, 0, 4, 2],
    [2, 1, 1, 1, 0, 0, 0, 0, 0, 0],
    [2, 0, 0, 0, 0, 0, 0, 0, 0, 2],
    [2, 0, 0, 0, 0, 0, 0, 3, 3, 2],
    [2, 0, 0, 0, 0, 0, 0, 3, 3, 2],
    [2, 0, 0, 0, 0, 0, 3, 3, 3, 2],
    [2, 2, 2, 2, 2, 2, 2, 2, 2, 2],
  ]
}));

world.addEntity(map);

let stairs = new Engine.Entity("stairs");

stairs.add(Components.Position(9, 4));
stairs.add(Components.Color("white"));
stairs.add(Components.Symbol(">"));

world.addEntity(stairs);

const element = rendering.canvas;
Input.Keyboard.attach(window);
Input.Mouse.attach(element);
Input.Gamepad.attach();

function tick() {
  world.update();
}

world.start();
setInterval(tick, 30);
document.body.appendChild(element);
