export default {
  tiles: {
    STONE: { fg: "grey", symbol: ".", walkable: true },
    WOOD: { fg: "#755c24", symbol: ".", walkable: true },
    WALL: { fg: "grey", symbol: "#" },
    WATER: { fg: "#2e80a7", bg: "#33a3d8", symbol: "~" },
    LAVA: { fg: "red", bg: "orange", symbol: "~" },
  },
  floor: {
    0: "STONE",
    1: "WOOD",
    2: "WALL",
    3: "WATER",
    4: "LAVA"
  }
}
