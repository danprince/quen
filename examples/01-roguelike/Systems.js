import { System, Filters } from "quen-engine";
import db from "./database";

export class RenderingSystem extends System {
  get filter() {
    return Filters.requireAll("symbol", "position");
  }

  constructor(config) {
    super();
    this.config = config;
    this.canvas = document.createElement("canvas");
    this.ctx = this.canvas.getContext("2d");
    this.resize(config.width, config.height);
  }

  resize(width, height) {
    this.canvas.width = width;
    this.canvas.height = height;
  }

  update() {
    let scale = this.config.scale;

    this.ctx.fillStyle = this.config.background;
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
    this.ctx.textAlign = "center";
    this.ctx.textBaseline = "middle";
    this.ctx.font = `${scale}px mononoki`;

    let map = this.getEntityByTag("map");

    for (let x = 0; x < map.grid.width; x++) {
      for (let y = 0; y < map.grid.height; y++) {
        this.ctx.save();
        let cell = map.grid.cells[y][x];
        let id = db.floor[cell];
        let tile = db.tiles[id];

        this.ctx.translate(x * scale, y * scale);
        this.ctx.fillStyle = tile.bg;
        this.ctx.fillRect(0, 0, scale, scale);
        this.ctx.fillStyle = tile.fg;
        this.ctx.translate(scale / 2, scale / 2);
        this.ctx.fillText(tile.symbol, 0, 0);
        this.ctx.restore();
      }
    }

    for (let entity of this.entities) {
      this.ctx.save();

      this.ctx.translate(
        entity.position.x * scale,
        entity.position.y * scale
      );

      if (entity.has("color")) {
        this.ctx.fillStyle = entity.color.background;
        this.ctx.fillRect(0, 0, scale, scale);
      }

      if (entity.has("color")) {
        this.ctx.translate(scale / 2, scale / 2);
        this.ctx.fillStyle = entity.color.foreground;
        this.ctx.fillText(entity.symbol.character, 0, 0);
      }

      this.ctx.restore();
    }
  }
}
