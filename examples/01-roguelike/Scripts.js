export const Player = {
  MoveLeft(self) {
    Player.Move(self, -1, 0);
  },

  MoveDown(self) {
    Player.Move(self, 0, 1);
  },

  MoveUp(self) {
    Player.Move(self, 0, -1);
  },

  MoveRight(self) {
    Player.Move(self, 1, 0);
  },

  Move(self, x, y) {
    let newX = self.position.x + x;
    let newY = self.position.y + y;

    let other = FindEntity(
      entity => entity.has("position"),
      entity => entity.position.x === newX && entity.position.y === newY
    );

    let map = GetByTag("map");
    let tile = map.grid.at(newX, newY);

    if (!tile.walkable) {
      return;
    }

    if (other) {
      Post(other.id, "Bump", self);
    } else {
      self.position.x = newX;
      self.position.y = newY;
    }
  }
};
